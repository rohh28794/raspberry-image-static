from gpiozero import *
import time

BtnAccept = Button(20)
BtnCancel = Button(21)
BtnFour = Button(16)
BtnThree = Button(6)
BtnTwo = Button(19)
BtnOne = Button(13)

Code = ["_","_","_","_"]

f = open("/home/pi/Desktop/team_a1/Project HoBoSecS/Code/KeypadCodes.txt", "r")
content = f.read()
KeypadCodes = content.splitlines()
f.close()

def ResetCode():
    CodeInput = ["_","_","_","_"]
    return CodeInput

def ValidateCode(CodeInput):
    for code in KeypadCodes:
        if ''.join(CodeInput) == code.split(',')[1]:
            print(f"Welcome {code.split(',')[0]}!")
            CodeInput = ResetCode
            time.sleep(0.25)
            accessStatus = True
            break
        else:
            accessStatus = False
    if accessStatus == False:
        print(f"Incorrect code: {''.join(Code)}!")
        Code = ["_","_","_","_"]
        time.sleep(0.25)

try:
    while True:
        if BtnAccept.is_pressed:
            ValidateCode(CodeInput)
        if BtnCancel.is_pressed:
            print("Code input is canceled!")
            Code = ["_","_","_","_"]
            time.sleep(0.25)
        if BtnFour.is_pressed:
            for idx, digit in enumerate(Code):
                if digit == '_':
                    Code[idx] = '4'
                    print(*Code)
                    time.sleep(0.25)
                    break
        if BtnThree.is_pressed:
            for idx, digit in enumerate(Code):
                if digit == '_':
                    Code[idx] = '3'
                    print(*Code)
                    time.sleep(0.25)
                    break
        if BtnTwo.is_pressed:
            for idx, digit in enumerate(Code):
                if digit == '_':
                    Code[idx] = '2'
                    print(*Code)
                    time.sleep(0.25)
                    break
        if BtnOne.is_pressed:
            for idx, digit in enumerate(Code):
                if digit == '_':
                    Code[idx] = '1'
                    print(*Code)
                    time.sleep(0.25)
                    break
except:
    print("Bye!")