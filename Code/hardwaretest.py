from gpiozero import *
from gpiozero.tones import Tone
import time

btnAccept = Button(24)
btnCancel = Button(5)
btnFour = Button(26)
btnThree = Button(6)
btnTwo = Button(13)
btnOne = Button(25)
ledGreen = LED(12)
ledRed = LED(20)
ledYellow = LED(18)
reedSwitch = Button(16, False)
passiveBuzzer = TonalBuzzer(19)

while True:
    print("Testing reed switch")
    while True:
        if reedSwitch.is_pressed:
            print("DOOR IS CLOSED!")
            break
        else:
            print("DOOR IS OPEN!")
        time.sleep(0.5)
    print("Testing buzzer!")
    passiveBuzzer.play(Tone("C4"))
    time.sleep(1)
    passiveBuzzer.play(Tone("C5"))
    time.sleep(1)
    passiveBuzzer.stop()
    print("Please press button 1")
    while True:
        if btnOne.is_pressed:
            print("Button 1 was pressed!\n")
            break
        else:
            continue
    print("Please press button 2")
    while True:
        if btnTwo.is_pressed:
            print("Button 2 was pressed!\n")
            break
        else:
            continue
    print("Please press button 3")
    while True:
        if btnThree.is_pressed:
            print("Button 3 was pressed!\n")
            break
        else:
            continue
    print("Please press button 4")
    while True:
        if btnFour.is_pressed:
            print("Button 4 was pressed!\n")
            break
        else:
            continue
    print("Please press button accept")
    while True:
        if btnAccept.is_pressed:
            print("Button accept was pressed!\n")
            break
        else:
            continue
    print("Please press button cancel")
    while True:
        if btnCancel.is_pressed:
            print("Button cancel was pressed!\n")
            break
        else:
            continue
    print("Testing green LED")
    ledGreen.blink(0.5,0.5,5,False)
    print("Testing red LED")
    ledRed.blink(0.5,0.5,5,False)
    print("Testing yellow LED")
    ledYellow.blink(0.5,0.5,5,False)
    break